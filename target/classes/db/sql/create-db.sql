--DROP TABLE users IF EXISTS;

CREATE TABLE users (
  email VARCHAR(40) PRIMARY KEY,
  firstName VARCHAR(30),
  lastName	VARCHAR(30),
  phone  VARCHAR(50)
);

CREATE TABLE Apps ( 
  AppId INTEGER PRIMARY KEY,
  AppName VARCHAR(30),
  AppDesc VARCHAR(30),
  emailId VARCHAR(30)
);

