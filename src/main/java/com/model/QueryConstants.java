package com.model;

public class QueryConstants {
    public static final String SQL_NEW_USER = "INSERT INTO Users(email, firstName, lastName, phoneNum) VALUES(:email, :firstName, :lastName, :phoneNum)";
    public static final String SQL_UPDATE_USER = "UPDATE Users SET firstName = :firstName, lastName = :lastName, phoneNum = :phoneNum WHERE email = :email";
    public static final String SQL_DELETE_USER = "DELETE FROM Users WHERE email = :email";
    public static final String SQL_RETRIEVE_USER = "SELECT * FROM Users where email=:email";
    public static final String SQL_REGISTER_APP = "INSERT INTO Apps(AppName , AppDesc, emailId) VALUES(:AppName,:AppDesc,:emailId)";
    public static final String SQL_RETRIEVE_USER_APPS = "SELECT * FROM Apps where emailId=:emailId";
	public static final String APP_ID = "appId";
}
