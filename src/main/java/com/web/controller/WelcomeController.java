package com.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.dao.UserDao;
import com.model.Application;
import com.model.User;

@RestController
public class WelcomeController {

	private static final Logger logger = LoggerFactory.getLogger(WelcomeController.class);

	@Autowired
	UserDao userDao;
	
	
	  @RequestMapping(value = "/user/", method = RequestMethod.POST)
	    public ResponseEntity<Void> createUser(@RequestBody User user,    UriComponentsBuilder ucBuilder) {
	        if (userDao.isUserExist(user.getEmail())) {
	            System.out.println("A User with mail " + user.getEmail() + " already exist");
	            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	        userDao.register(user);
	        HttpHeaders headers = new HttpHeaders();
	        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getEmail()).toUri());
	        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	    }
	  
	   @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<User> getUser(@PathVariable("id") String id) {
	        System.out.println("Fetching User with id " + id);
	        User user = userDao.retrieveUserInfo(id);
	        if (user == null) {
	            System.out.println("User with id " + id + " not found");
	            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	        }
	        return new ResponseEntity<User>(user, HttpStatus.OK);
	    }
	   
	   @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	    public ResponseEntity<User> updateUser(@PathVariable("id") String id, @RequestBody User user) {
	        System.out.println("Updating User " + id);
	        User currentUser = userDao.retrieveUserInfo(id);
	        if (currentUser==null) {
	            System.out.println("User with id " + id + " not found");
	            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	        }
	        currentUser.setFirstName(user.getFirstName());
	        currentUser.setEmail(user.getEmail());
	        currentUser.setLastName(user.getLastName());
	        currentUser.setPhone(user.getPhone());
	        userDao.updateRegistration(currentUser);
	        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
	    }
	   
	   @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<User> deleteUser(@PathVariable("id") String id) {
	        System.out.println("Fetching & Deleting User with id " + id);
	        User user = userDao.retrieveUserInfo(id);
	        if (user == null) {
	            System.out.println("Unable to delete. User with id " + id + " not found");
	            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	        }
	        userDao.deleteRegistration(id);
	        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	    }
	   
	   @RequestMapping(value = "/user/{id}/app/{appId}", method = RequestMethod.POST)
	    public ResponseEntity<Integer> createApp(@RequestBody Application app, @PathVariable("id") String id,   UriComponentsBuilder ucBuilder) {
		    app = userDao.registerApp(app);
	        HttpHeaders headers = new HttpHeaders();
	        headers.setLocation(ucBuilder.path("/user/{id}/app/{appId}").buildAndExpand(id,app.getAppId()).toUri());
	        return new ResponseEntity<Integer>(headers, HttpStatus.CREATED);
	    }
	   
	   @RequestMapping(value = "/user/{id}/app", method = RequestMethod.GET)
	    public ResponseEntity<List<Application>> listAllUsers(@PathVariable("id") String id) {
	        List<Application> apps = userDao.retrieveAppList(id);
	        if(apps.isEmpty()){
	            return new ResponseEntity<List<Application>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	        }
	        return new ResponseEntity<List<Application>>(apps, HttpStatus.OK);
	    }
}