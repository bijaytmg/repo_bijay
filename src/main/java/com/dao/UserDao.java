package com.dao;

import java.util.List;

import com.model.Application;
import com.model.User;

public interface UserDao {

	User findByName(String name);
	
	List<User> findAll();
	
	User register(User user);
	
	User retrieveUserInfo(String email);
	
	User updateRegistration(User user);
	
	int deleteRegistration(String email);
	
	Application registerApp(Application app);
	
	List<Application> retrieveAppList(String email);
	
	boolean isUserExist(String email);
}