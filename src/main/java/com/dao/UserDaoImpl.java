package com.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.model.Application;
import com.model.QueryConstants;
import com.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public User findByName(String name) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        
		String sql = "SELECT * FROM users WHERE name=:name";
		
        User result = namedParameterJdbcTemplate.queryForObject(
                    sql,
                    params,
                    new UserMapper());
                    
        return result;
        
	}

	@Override
	public List<User> findAll() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		String sql = "SELECT * FROM users";
		
        List<User> result = namedParameterJdbcTemplate.query(sql, params, new UserMapper());
        
        return result;
        
	}

	private static final class UserMapper implements RowMapper<User> {

		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			//user.setId(rs.getInt("id"));
			//user.setName(rs.getString("name"));
			user.setEmail(rs.getString("email"));
			user.setFirstName(rs.getString("firstName"));
			user.setLastName(rs.getString("lastName"));
			user.setPhone(rs.getString("phone"));
			return user;
		}
	}

	@Override
	public User register(User user) {
		//    public static final String SQL_NEW_USER = "INSERT INTO Users(email, firstName, lastName, phoneNum) VALUES(:email, :firstName, :lastName, :phoneNum)";
	  try{
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", user.getEmail());
        params.put("firstName", user.getFirstName());
        params.put("lastName", user.getLastName());
        params.put("phoneNum", user.getPhone());
		namedParameterJdbcTemplate.update(QueryConstants.SQL_NEW_USER, params);
	  }catch(Exception e){
		  return null;
	  }
      return user;
	}

	@Override
	public User retrieveUserInfo(String email) {
	    //public static final String SQL_RETRIEVE_USER = "SELECT * FROM Users where email=:email";
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", email);
        
        User result = null;
		try {
			namedParameterJdbcTemplate.queryForObject(QueryConstants.SQL_RETRIEVE_USER, params, new UserMapper());
		} catch (Exception e) {
			
		}
                    
        return result;
	}

	@Override
	public User updateRegistration(User user) {
		 try{
		//  public static final String SQL_UPDATE_USER = "UPDATE Users SET firstName = :firstName, lastName = :lastName, phoneNum = :phoneNum WHERE email = :email";
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("email", user.getEmail());
	        params.put("firstName", user.getFirstName());
	        params.put("lastName", user.getLastName());
	        params.put("phoneNum", user.getPhone());
			namedParameterJdbcTemplate.update(QueryConstants.SQL_UPDATE_USER, params);
		  }catch(Exception e){
			  return null;
		  }
	      return user;
	}

	@Override
	public int deleteRegistration(String email) {
		// public static final String SQL_DELETE_USER = "DELETE FROM Users WHERE
		// email = :email";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("email", email);
			namedParameterJdbcTemplate.update(QueryConstants.SQL_UPDATE_USER, params);
		} catch (Exception e) {
			return 0;
		}
		return 1;
	}

	@Override
	public Application registerApp(Application app) {
		//    public static final String SQL_REGISTER_APP = "INSERT INTO Apps(AppName , AppDesc, emailId) VALUES(:AppName,:AppDesc,:emailId)";
		try{
			
			GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();

	        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
	        namedParameters.addValue("AppName", app.getAppName());
	        namedParameters.addValue("AppDesc", app.getAppDesc());
	        namedParameters.addValue("emailId", app.getEmailId());
	        
			/*Map<String, Object> params = new HashMap<String, Object>();
			KeyHolder holder = new GeneratedKeyHolder();
	        params.put("AppName", app.getAppName());
	        params.put("AppDesc", app.getAppDesc());
	        params.put("emailId", app.getEmailId());*/
			namedParameterJdbcTemplate.update(QueryConstants.SQL_REGISTER_APP, namedParameters, generatedKeyHolder);
			app.setAppId(String.valueOf(generatedKeyHolder.getKey().intValue()));
			//namedParameterJdbcTemplate.
		  }catch(Exception e){
			  return null;
		  }
	      return app;
	}

	@Override
	public List<Application> retrieveAppList(String email) {
		// public static final String SQL_RETRIEVE_USER_APPS = "SELECT * FROM Apps where emailId=:emailId";
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("emailId", email);
        List<Application> result = namedParameterJdbcTemplate.query(QueryConstants.SQL_RETRIEVE_USER_APPS,
                    params,
                    new AppMapper());
		return result;
	}
	
	private static final class AppMapper implements RowMapper<Application> {

		public Application mapRow(ResultSet rs, int rowNum) throws SQLException {
			Application app = new Application();
			app.setAppName(rs.getString("AppName"));
			app.setAppDesc(rs.getString("AppDesc"));
			return app;
		}
	}

	@Override
	public boolean isUserExist(String email) {
		return retrieveUserInfo(email)==null?false:true;
	}
	
}