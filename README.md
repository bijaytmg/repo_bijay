Spring Embedded Database
===============================
Template for a Spring 4 MVC + Embedded Database examples, using HSQLDB, H2 and Derby.

###1. Technologies used
* Maven 3.0
* Spring 4.1.6.RELEASE
* HSQLDB 2.3.2
* H2 1.4.187
* Derby 10.11.1.1

###2. To import this project into Eclipse IDE
1. ```$ mvn eclipse:eclipse```
2. Import into Eclipse via **existing projects into workspace** option.
3. Done.

